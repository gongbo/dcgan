import tensorflow as tf
import time

a = tf.Variable(tf.random_uniform([10000, 10000]))

sess = tf.Session()
init = tf.global_variables_initializer()
sess.run(init)

start= time.time()

sess.run(tf.matrix_inverse(a))
print(time.time() - start)
#
import numpy as np
#
# a = np.random.random([10000,10000])
# start = time.time()
# b = np.linalg.inv(a)

# print(time.time() - start)